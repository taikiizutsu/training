#coding: UTF-8
import io
import os
import logging
import datetime


from flask import render_template, request, Flask
from google.cloud import storage
from google.cloud.storage import Blob
from PIL import Image


app = Flask(__name__)
logging.getLogger().setLevel(logging.DEBUG)

now = datetime.date.today()

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'GET':
        client = storage.Client()
        bucket = client.get_bucket('turing-mark-276307')
        return render_template('index.html', blobs=bucket.list_blobs())

    elif request.method == 'POST':
        if 'btnform1' in request.form:
            file = request.files['file'].read()
            client = storage.Client()        
            bucket = client.get_bucket('turing-mark-276307')
            img_resize = Converter(file)   
            bio = io.BytesIO()
            img_resize.save(bio, 'jpeg')
            filename = Filename()
            blob = Blob(filename, bucket)
            blob.upload_from_string(data=bio.getvalue(), content_type="image/jpeg")
            return 'アップロードしました。'
        
    

def Filename():
    client = storage.Client()
    bucket = client.get_bucket('turing-mark-276307')
    count = 1
    filename = str(now) + '-' + str(count) + '.jpg'
    blobs = bucket.list_blobs()
    for blob in blobs:
        if filename == blob.name:
            count += 1
            filename = str(now) + '-' + str(count) + '.jpg'
            
    return filename

def Converter(file):  
    img_bin = io.BytesIO(file)
    img = Image.open(img_bin)
    img.load()
    image2 = Image.new("RGB", img.size, (255, 255, 255))
    image2.paste(img, mask=img.split()[3])
    image2 = MakeRectangle(image2)
 
    return image2

 
def MakeRectangle(im):
    width = im.width
    height = im.height
    back = Image.new("RGB", (900, 540))
    if width == height:
        im_resize = im.resize((540, 540 ))
        back.paste(im_resize, (180, 0))
        return back

    elif width <= 900 and height > 540:
        k = 540/height
        im_resize = im.resize((int(width*k), 540))
        new_w = (900-width*k)/2
        back.paste(im_resize, (int(new_w), 0))
        return back
        
    elif width > 900 and height <= 540:
        k = 900/width
        im_resize = im.resize((900, int(height*k)))
        new_h = (540-height*k)/2
        back.paste(im_resize, (0, int(new_h)))
        return back

    elif width > 900 and height > 540 or width <=900 and height <=540:
        fx = 900/width
        fy = 540/height
        if fx > fy:
            new_w = width*fy
            new_h = height*fy
            renew_w = (900-new_w)/2

        elif fy > fx:
            new_w = width*fx
            new_h = height*fx
            renew_h = (540-new_h)/2

        im_resize = im.resize((int(new_w), int(new_h)))
        
        if fx > fy:
            back.paste(im_resize, (int(renew_w), 0))
        elif fy > fx:
            back.paste(im_resize, (0, int(renew_h)))

        return back     


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
